package com.example.myfirstcopyofyoutubeprojectbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirstCopyOfYoutubeProjectBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyFirstCopyOfYoutubeProjectBeApplication.class, args);
    }

}